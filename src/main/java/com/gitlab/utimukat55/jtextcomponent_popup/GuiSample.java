/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.jtextcomponent_popup;

import java.awt.BorderLayout;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

/**
 * Usage sample program for JTextComponent-popup.
 * @author utimukat55
 */
public class GuiSample extends JDialog {

	private static final long serialVersionUID = 3797442785689703097L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtField;
	private JTextField txtPopupField;
	private JTextArea textArea;
	private JTextArea popupTextArea;
	private JEditorPane editorPane;
	private JEditorPane popupEditorPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			GuiSample dialog = new GuiSample();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public GuiSample() {
		JLabel lblJtextfield = new JLabel("JTextField");
		JLabel lblJpopuptextfield = new JLabel("JPopupTextField");
		JLabel lblJtextarea = new JLabel("JTextArea");
		JLabel lblJpopuptextarea = new JLabel("JPopupTextArea");
		JLabel lblJeditorpane = new JLabel("JEditorPane");
		JLabel lblJpopupeditorpane = new JLabel("JPopupEditorPane");
		
		JScrollPane scrTextArea = new JScrollPane();
		JScrollPane scrPopupText = new JScrollPane();
		JScrollPane scrEditorPane = new JScrollPane();
		JScrollPane scrPopupEditor = new JScrollPane();

		setTitle("Popup sample for JTextField, JTextArea, JEditorPane");
		setBounds(100, 100, 520, 480);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		txtField = new JTextField("JTextField");
		txtField.setColumns(20);
		
		txtPopupField = new JPopupTextField("JPopupTextField(right click)");
		txtPopupField.setColumns(20);
		
		editorPane = new JEditorPane("text/html", "<b>JEditorPane</b>");
		scrEditorPane.setViewportView(editorPane);

		popupEditorPane = new JPopupEditorPane("text/html", "<b>JPopupEditorPane</b><br>(right click)");
		scrPopupEditor.setViewportView(popupEditorPane);
		
		textArea = new JTextArea("JTextArea");
		scrTextArea.setViewportView(textArea);

		popupTextArea = new JPopupTextArea("JPopupTextArea\n(right click)");
		scrPopupText.setViewportView(popupTextArea);
		
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(scrTextArea)
						.addComponent(txtField)
						.addComponent(lblJtextfield)
						.addComponent(lblJtextarea)
						.addComponent(lblJeditorpane)
						.addComponent(scrEditorPane))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblJpopupeditorpane)
						.addComponent(lblJpopuptextarea)
						.addComponent(lblJpopuptextfield)
						.addComponent(txtPopupField)
						.addComponent(scrPopupText)
						.addComponent(scrPopupEditor))
					.addContainerGap(40, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblJtextfield)
						.addComponent(lblJpopuptextfield))
					.addGap(6)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtPopupField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblJtextarea)
						.addComponent(lblJpopuptextarea))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrPopupText, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrTextArea, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblJeditorpane)
						.addComponent(lblJpopupeditorpane))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrEditorPane, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrPopupEditor, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
					.addGap(263))
		);
		contentPanel.setLayout(gl_contentPanel);
	}
}

/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.jtextcomponent_popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.gitlab.utimukat55.jtextcomponent_popup.internal.JPopupConstants;
import com.gitlab.utimukat55.jtextcomponent_popup.internal.JPopupUtil;

/**
 * JEditorPane with cut/copy/paste functions.
 * 
 * @see javax.swing.JEditorPane
 * @author utimukat55
 */
public class JPopupEditorPane extends JEditorPane implements ActionListener {

	private static final long serialVersionUID = -6005966149281154623L;
	private JMenuItem cut;
	private JMenuItem copy;
	private JMenuItem paste;
	private JPopupMenu menu;

	private String LABEL_CUT;
	private String LABEL_COPY;
	private String LABEL_PASTE;

	/**
	 * {@inheritDoc}
	 */
	public JPopupEditorPane() {
		super();
		setupMenuLabel();
		setupPopupMenu();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public JPopupEditorPane(URL initialPage) throws IOException {
		super(initialPage);
		setupMenuLabel();
		setupPopupMenu();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public JPopupEditorPane(String url) throws IOException {
		super(url);
		setupMenuLabel();
		setupPopupMenu();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public JPopupEditorPane(String type, String text) {
		super(type, text);
		setupMenuLabel();
		setupPopupMenu();
	}
	
	private void setupMenuLabel() {
		LABEL_CUT = JPopupUtil.getCutLabelByLocale();
		LABEL_COPY = JPopupUtil.getCopyLabelByLocale();
		LABEL_PASTE = JPopupUtil.getPasteLabelByLocale();
	}
	private void setupPopupMenu() {
		cut = new JMenuItem(LABEL_CUT);
		cut.setMnemonic(JPopupConstants.NEMONIC_CUT);
		cut.addActionListener(this);

		copy = new JMenuItem(LABEL_COPY);
		copy.setMnemonic(JPopupConstants.NEMONIC_COPY);
		copy.addActionListener(this);

		paste = new JMenuItem(LABEL_PASTE);
		paste.setMnemonic(JPopupConstants.NEMONIC_PASTE);
		paste.addActionListener(this);

		menu = new JPopupMenu();
		menu.add(cut);
		// menu.addSeparator(); // uncomment if seprator needs between copy and paste.
		menu.add(copy);
		menu.add(paste);
		setComponentPopupMenu(menu);
	}

	/**
	 * Function definition.
	 * 
	 * @param e event where fired.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cut) {
			// cut to clipboard.
			cut();
		} else if (e.getSource() == copy) {
			// copy to clipboard.
			copy();
		} else if (e.getSource() == paste) {
			// paste to TextField.
			paste();
		}
	}



	
}

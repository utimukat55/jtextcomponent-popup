/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.jtextcomponent_popup.internal;

import java.awt.event.KeyEvent;

/**
 * Constants class for JTextComponent-popup.
 * @author utimukat55
 *
 */
public class JPopupConstants {
	private JPopupConstants() {
	}
	
	/**
	 * Keycode for X.
	 */
	public static final int NEMONIC_CUT = KeyEvent.VK_X;

	/**
	 * Keycode for C.
	 */
	public static final int NEMONIC_COPY = KeyEvent.VK_C;

	/**
	 * Keycode for V.
	 */
	public static final int NEMONIC_PASTE = KeyEvent.VK_V;

	/**
	 * English label for cut.
	 */
	public static final String LABEL_CUT = "Cut(X)";

	/**
	 * English label for copy.
	 */
	public static final String LABEL_COPY = "Copy(C)";

	/**
	 * English label for paste.
	 */
	public static final String LABEL_PASTE = "Paste(V)";

	/**
	 * Japanese label for cut.
	 */
	public static final String LABEL_CUT_JA = "切り取り(X)";

	/**
	 * Japanese label for copye.
	 */
	public static final String LABEL_COPY_JA = "コピー(C)";

	/**
	 * Japanese label for paste.
	 */
	public static final String LABEL_PASTE_JA = "貼り付け(V)";


}

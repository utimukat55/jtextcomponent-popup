/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.jtextcomponent_popup;

/**
 * Run Sample Dialog App.
 *
 */
public class App {
	public static void main(String[] args) {
		GuiSample.main(args);
	}
}

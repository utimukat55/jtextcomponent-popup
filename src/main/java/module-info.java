/**
 * JTextComponent-popup
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

module com.gitlab.utimukat55.jtextcomponentpopup {
	requires transitive java.desktop;
	exports com.gitlab.utimukat55.jtextcomponent_popup;
}
# JTextComponent-popup

JTextArea, JTextField, JEditorPane with popup menu (right button clicked menu).

[![](https://jitpack.io/v/com.gitlab.utimukat55/jtextcomponent-popup.svg)](https://jitpack.io/#com.gitlab.utimukat55/jtextcomponent-popup)

# Prerequisites

- Java8+
- Maven installed PC

# How to use

## Maven build

To run sample program in repository, please clone this repository. After clone, execute commands below.

```
$ cd jtextcomponent-popup/jtextcomponent-popup
$ mvn package
```

These commands make `jtextcomponent-popup-0.0.1.jar` in `target` directory.

## Run sample

To check sample app, execute commands below.

```
$ cd target
$ java -cp jtextcomponent-popup-0.0.1.jar com.gitlab.utimukat55.jtextcomponent_popup.App
```

![Popup sample app](./sampleapp.png)

On the right side of this program is sample of JPopupTextField, JPopupTextArea, JPopupEditorPane. You can try them.

Popup menu's label is displayed on your locale(first release corresponds English and Japanese).

## Coding

Replacing the code to use JTextComponent-popup. These classes are thin sub-classes of JTextArea, JTextField, JEditorPane. Sample code is below.

```java
private JTextField txtPopupField;
txtPopupField = new JPopupTextField("JPopupTextField(right click)");
```

# Import to your project

## Reference via maven

JTextComponent-Popup can reference via maven using JitPack. Please add 2 blocks in your project.

```
	<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>
```

```
	<dependency>
	    <groupId>com.gitlab.utimukat55</groupId>
	    <artifactId>jtextcomponent-popup</artifactId>
	    <version>0.0.1</version>
	</dependency>
```

## Import as jar

If your Java project is JSR376 moduled or not, you can import as module jar and as old-school library jar.

## Import as java source files

JTextComponent-popup is licensed by CC0, you can use too many ways. If you want to use only JPopupTextField class, please copy files below.

- internal/JPopupConstants.java
- internal/JPopupUtil.java
- JPopupTextField.java

# Improve this project

I18n is needed. I want the words "cut" "copy" "paste" of many languages. Please MR.

# License

JTextComponent-popup is licensed by CC0 ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed)).